# 联系方式

- 手机：13520646170
- Email：xiaoyueyue165@gmail.com
- 微信号：YANYUE165

# 个人信息

 - 闫越/男/1994 
 - 本科/晋中学院计算机系 
 - 工作年限：3年
 - 技术博客：https://github.com/xiaoyueyue165/blog
 - Github：https://github.com/xiaoyueyue165
 - 期望职位：Web前端高级程序员
 - 期望薪资：税前月薪15k，特别喜欢的公司可例外
 - 期望城市：北京

# 技能清单

以下均为我熟练使用的技能

- Web开发：Native javascript
- 前端框架技术栈：
  + Vue + Vue-router + Vuex
  + React + React-router + Redux + React-redux
- 前端工具：Webpack/Sass/Babel/Fis3/Json-server/Postman/Eslint
- 版本管理、文档和自动化部署工具：Svn/Git/PHPDoc/Phing/Composer


## 参考技能关键字

- javascript(596)
- css(555)
- github(409)
- html(430)
- http(382)
- jquery(323)
- html5(312)
- git(277)
- ajax(196)
- css3(176)
- dom(63)
- bootstrap(43)
- chrome(28)
- nodejs(28)
- angularjs(25)
- mobile(22)
- sass(17)

# 工作经历
（工作经历按逆序排列，最新的在最前边，按公司做一级分组，公司内按二级分组）

## ABC公司 （ 2012年9月 ~ 2014年9月 ）

### DEF项目 
我在此项目负责了哪些工作，分别在哪些地方做得出色/和别人不一样/成长快，这个项目中，我最困难的问题是什么，我采取了什么措施，最后结果如何。这个项目中，我最自豪的技术细节是什么，为什么，实施前和实施后的数据对比如何，同事和领导对此的反应如何。


### GHI项目 
我在此项目负责了哪些工作，分别在哪些地方做得出色/和别人不一样/成长快，这个项目中，我最困难的问题是什么，我采取了什么措施，最后结果如何。这个项目中，我最自豪的技术细节是什么，为什么，实施前和实施后的数据对比如何，同事和领导对此的反应如何。


### 其他项目

（每个公司写2~3个核心项目就好了，如果你有非常大量的项目，那么按分类进行合并，每一类选一个典型写出来。其他的一笔带过即可。）

 
## JKL公司 （ 2010年3月 ~ 2012年8月 ）

### MNO项目 
我在此项目负责了哪些工作，分别在哪些地方做得出色/和别人不一样/成长快，这个项目中，我最困难的问题是什么，我采取了什么措施，最后结果如何。这个项目中，我最自豪的技术细节是什么，为什么，实施前和实施后的数据对比如何，同事和领导对此的反应如何。


### PQR项目 
我在此项目负责了哪些工作，分别在哪些地方做得出色/和别人不一样/成长快，这个项目中，我最困难的问题是什么，我采取了什么措施，最后结果如何。这个项目中，我最自豪的技术细节是什么，为什么，实施前和实施后的数据对比如何，同事和领导对此的反应如何。


### 其他项目

（每个公司写2~3个核心项目就好了，如果你有非常大量的项目，那么按分类进行合并，每一类选一个典型写出来。其他的一笔带过即可。）


# 开源项目和作品
（这一段用于放置工作以外的、可证明你的能力的材料）

## 开源项目

 - [STU](http://github.com/yourname/projectname)：项目的简要说明，Star和Fork数多的可以注明
 - [WXYZ](http://github.com/yourname/projectname)：项目的简要说明，Star和Fork数多的可以注明

## 技术文章

- [git+Github的正确姿势 ](https://github.com/xiaoyueyue165/blog/issues/2)
- [前端模拟api数据的两种方式](https://github.com/xiaoyueyue165/blog/issues/25)
- [一张网页，要经历怎样的过程，才能抵达用户面前？](https://github.com/xiaoyueyue165/blog/blob/master/docs/%E4%B8%80%E5%BC%A0%E7%BD%91%E9%A1%B5%EF%BC%8C%E8%A6%81%E7%BB%8F%E5%8E%86%E6%80%8E%E6%A0%B7%E7%9A%84%E8%BF%87%E7%A8%8B%EF%BC%8C%E6%89%8D%E8%83%BD%E6%8A%B5%E8%BE%BE%E7%94%A8%E6%88%B7%E9%9D%A2%E5%89%8D%EF%BC%9F.md)

# 致谢
感谢您花时间阅读我的简历，期待能有机会和您共事。
