<!doctype html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2">
    <link rel="shortcut icon" href="http://static.smartisanos.cn/common/img/favicon.ico">
    <title>Coming soon...</title>

    <style>

        .come {
            position: absolute;
            left: 0;
            right: 0;
            top:0;
            bottom: 0;
            width: 1000px;
            margin: auto;
            background: url("http://static.smartisanos.cn/common/img/coming_soon.png") no-repeat center;
            -webkit-background-size: contain;
            background-size: contain;
        }

        @media screen and (min-width: 1px) {
            .come {
                width: 75%;
                max-width: 1000px;
            }
        }
    </style>

    <!--[if lte IE 7]>
    <script src="http://static.smartisanos.cn/common/js/update-browser.js"></script>
    <![endif]-->
</head>
<body>
    <div class="come"></div>
    <!-- Analytics -->
<script>
    // Google
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-49819160-1', 'smartisan.com');
    ga('send', 'pageview');

    // Baidu
    var _hmt = _hmt || [];
    (function() {
      var hm = document.createElement("script");
      hm.src = "//hm.baidu.com/hm.js?482e5a238e881dd62851ca123d8b11f3";
      var s = document.getElementsByTagName("script")[0]; 
      s.parentNode.insertBefore(hm, s);
    })();

</script>
</body>
</html>
